package mx.x10.ampers.openspritzbible.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SeekBarPreference;

import mx.x10.ampers.openspritzbible.R;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends PreferenceFragmentCompat {

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState,
                                    String rootKey) {

        setPreferencesFromResource(R.xml.preferences, rootKey);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View view = super.onCreateView(inflater, container, savedInstanceState);
        //view.setBackgroundColor(getResources().getColor(android.R.color.white));


        return view;

    }


    @Override
    public void onStop() {
        super.onStop();

        SeekBarPreference settingsWPM = findPreference("seekBarWPM");
        SeekBarPreference settingsTextSize = findPreference("seekBarTextSize");

        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putInt("seekBarWPM", settingsWPM.getValue()).apply();
        editor.putInt("seekBarTextSize", settingsTextSize.getValue()).apply();

        editor.commit();

    }

    @Override
    public void onPause(){
        super.onPause();

        SeekBarPreference settingsWPM = findPreference("seekBarWPM");
        SeekBarPreference settingsTextSize = findPreference("seekBarTextSize");

        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putInt("seekBarWPM", settingsWPM.getValue()).apply();
        editor.putInt("seekBarTextSize", settingsTextSize.getValue()).apply();

        editor.commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        //super.onCreateOptionsMenu(menu,inflater);
    }

}