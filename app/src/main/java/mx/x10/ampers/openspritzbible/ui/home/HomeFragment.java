package mx.x10.ampers.openspritzbible.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.andrewgiang.textspritzer.lib.Spritzer;
import com.andrewgiang.textspritzer.lib.SpritzerTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mx.x10.ampers.openspritzbible.R;

public class HomeFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( getContext() );
        String passageTitleText = prefs.getString("pref_pastPassage", getResources().getString(R.string.enterPassage));
        String pref_passageText = prefs.getString("pref_pastPassageText", getResources().getString(R.string.welcomeMessage));

        ToggleButton viewVerseText = (ToggleButton) root.findViewById(R.id.viewVerseText);
        TextView verseTextArea = (TextView) root.findViewById(R.id.verseTextArea);
        ScrollView verseScrollArea = (ScrollView) root.findViewById(R.id.scrollView);
        EditText passageText = (EditText) root.findViewById(R.id.searchText);
        Button searchButton = (Button) root.findViewById(R.id.btnSearch);
        TextView viewWPM = (TextView) root.findViewById(R.id.viewWPM);
        SpritzerTextView mSpritzerTextView = (SpritzerTextView) root.findViewById(R.id.spritzTV);
        ProgressBar mProgressBar = (ProgressBar) root.findViewById(R.id.spritz_progress);

        passageText.setText(passageTitleText);
        verseTextArea.setText(pref_passageText);
        mSpritzerTextView.setSpritzText(pref_passageText);

        mSpritzerTextView.setTextSize(prefs.getInt("seekBarTextSize", 30));
        mSpritzerTextView.setWpm(prefs.getInt("seekBarWPM", 500));
        mSpritzerTextView.attachProgressBar(mProgressBar);

        viewWPM.setText("WPM: " + prefs.getInt("seekBarWPM", 500) );

        verseTextArea.setMovementMethod(new ScrollingMovementMethod());

        //Set Click Control listeners, these will be called when the user uses the click controls
        mSpritzerTextView.setOnClickControlListener(new SpritzerTextView.OnClickControlListener() {
            @Override
            public void onPause() {

                Animation animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fadein);

                passageText.setAnimation(animFadeIn);
                passageText.setVisibility(View.VISIBLE);

                searchButton.setAnimation(animFadeIn);
                searchButton.setVisibility(View.VISIBLE);

                viewWPM.setAnimation(animFadeIn);
                viewWPM.setVisibility(View.VISIBLE);

                viewVerseText.setAnimation(animFadeIn);
                viewVerseText.setVisibility(View.VISIBLE);

                if (viewVerseText.isChecked()){
                    verseScrollArea.setAnimation(animFadeIn);
                    verseScrollArea.setVisibility(View.VISIBLE);
                }


                Toast.makeText(getContext(), "Paused", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onPlay() {

                Animation animFadeOut =AnimationUtils.loadAnimation(getContext(), R.anim.fadeout);

                passageText.setAnimation(animFadeOut);
                passageText.setVisibility(View.INVISIBLE);

                searchButton.setAnimation(animFadeOut);
                searchButton.setVisibility(View.INVISIBLE);

                viewWPM.setAnimation(animFadeOut);
                viewWPM.setVisibility(View.INVISIBLE);

                viewVerseText.setAnimation(animFadeOut);
                viewVerseText.setVisibility(View.INVISIBLE);

                if (viewVerseText.isChecked()){
                    verseScrollArea.setAnimation(animFadeOut);
                    verseScrollArea.setVisibility(View.INVISIBLE);
                }

                Toast.makeText(getContext(), "Playing", Toast.LENGTH_SHORT).show();
            }
        });

        mSpritzerTextView.setOnCompletionListener(new Spritzer.OnCompletionListener() {
            @Override
            public void onComplete() {
                Animation animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fadein);

                passageText.setAnimation(animFadeIn);
                passageText.setVisibility(View.VISIBLE);

                searchButton.setAnimation(animFadeIn);
                searchButton.setVisibility(View.VISIBLE);

                viewWPM.setAnimation(animFadeIn);
                viewWPM.setVisibility(View.VISIBLE);

                viewVerseText.setAnimation(animFadeIn);
                viewVerseText.setVisibility(View.VISIBLE);

                if (viewVerseText.isChecked()){
                    verseScrollArea.setAnimation(animFadeIn);
                    verseScrollArea.setVisibility(View.VISIBLE);
                }

                Toast.makeText(getContext(), "Passsage is finished", Toast.LENGTH_SHORT).show();

            }
        });



        final String rootURL = "https://api.esv.org/v3/passage/text/?q=";
        String endURL ="include-passage-references=false&include-footnotes=false&include-audio-link=false&include-passage-references=false&include-short-copyright=false&include-headings=false&include-verse-numbers=false&indent-poetry-lines=2";

        searchButton.setOnClickListener(view -> {

            getBiblePassageTask getPassageData = new getBiblePassageTask(result -> displayJSONcontent(result, root), getActivity());

            getPassageData.execute(rootURL + Uri.encode(passageText.getText().toString()) + endURL);

        });


        viewVerseText.setOnClickListener (new View.OnClickListener() {
            public void onClick(View v) {

                if (viewVerseText.isChecked())
                    verseScrollArea.setVisibility(View.VISIBLE);
                else
                    verseScrollArea.setVisibility(View.INVISIBLE);

            }
        });


        verseTextArea.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //to do
                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText(verseTextArea.getText().toString() );
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("passage",verseTextArea.getText().toString());
                    clipboard.setPrimaryClip(clip);
                }

                Toast.makeText(getContext(), "Verse(s) copied to clipboard", Toast.LENGTH_SHORT).show();

                return true;
            }
        });


        return root;
    }

    // code taken from the Inductive Bible Study app
    private void displayJSONcontent(String result, View root) {

        if(result != null) {
            try {
                // https://api.esv.org/docs/passage-html/
                JSONArray textPassage =new JSONObject(result).getJSONArray("passages");
                String passageTitle =new JSONObject(result).getString("canonical");
                String formattedText = textPassage.getString(0).replace("\n  \n  \n    ", "\n\n ");
                //String formattedText = textPassage.getString(0);

                Log.i("text is: ", result);

                TextView verseTextArea = (TextView) root.findViewById(R.id.verseTextArea);
                verseTextArea.setText(formattedText.toString());


                EditText passageTitleText = (EditText) root.findViewById(R.id.searchText);
                passageTitleText.setText(passageTitle.toString());

                // Save last passaged entered in the text box
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( getContext() );
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("pref_pastPassage", passageTitleText.getText().toString() );
                editor.putString("pref_pastPassageText",formattedText);
                editor.commit();

                SpritzerTextView mSpritzerTextView = (SpritzerTextView) root.findViewById(R.id.spritzTV);
                mSpritzerTextView.setSpritzText(String.valueOf(formattedText));

                TextView passageCopyright = root.findViewById(R.id.passageCopyright);
                passageCopyright.setText(getString(R.string.ESVcopyright));


            }
            catch(JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public interface FragmentCallback {
        public void onTaskDone(String result);
    }


}