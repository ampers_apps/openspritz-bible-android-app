package mx.x10.ampers.openspritzbible.ui.about;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceFragmentCompat;

import mx.x10.ampers.openspritzbible.R;

public class AboutFragment extends PreferenceFragmentCompat {

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState,
                                    String rootKey) {

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View root = inflater.inflate(R.layout.fragment_about, container, false);

        TextView contact_me = (TextView) root.findViewById(R.id.textContact);
        contact_me.setMovementMethod(LinkMovementMethod.getInstance());
        contact_me.setText(Html.fromHtml(getString(R.string.contact_me)));

        TextView about_text = (TextView) root.findViewById(R.id.textAbout);
        about_text.setMovementMethod(LinkMovementMethod.getInstance());
        about_text.setText(Html.fromHtml(getString(R.string.about_text)));


        return root;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        //super.onCreateOptionsMenu(menu,inflater);
    }

}