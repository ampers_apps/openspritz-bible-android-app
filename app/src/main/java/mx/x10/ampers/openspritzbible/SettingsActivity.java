package mx.x10.ampers.openspritzbible;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import mx.x10.ampers.openspritzbible.ui.settings.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

}
