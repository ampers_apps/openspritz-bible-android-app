/**
 * Automatically generated file. DO NOT MODIFY
 */
package mx.x10.ampers.openspritzbible;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "mx.x10.ampers.openspritzbible";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 5;
  public static final String VERSION_NAME = "2.0.0";
}
